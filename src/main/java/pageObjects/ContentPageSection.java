package pageObjects;


import base.TestUtilities;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**/
public class ContentPageSection extends BasePageObject {

    By itemLocator =By.xpath("//div[@class='product-container']");
    By addToCartBtnLocator =By.xpath("//a[@title='Add to cart']");
    By closeIconLocator =By.xpath("//span[@title='Close window']");
    By checkOutBtnLocator = By.xpath("//*[@id='layer_cart']/div[1]/div[2]/div[4]/a/span");
    By checkSearchResultLocator = By.xpath("//*[@id='center_column']/h1/span");
    By productLbtLocator = By.cssSelector("#cart_summary > thead > tr > th.cart_product.first_item");
    By removeIconLocator = By.cssSelector("a[title='Delete'] > .icon-trash");
    By shoppingCartMessageLocator = By.cssSelector("div#center_column > .alert.alert-warning");

    public ContentPageSection(WebDriver driver, Logger log) {
        super(driver, log);
    }

    //This method add a new item on the shopping cart
    public void addItem(int itemPosition){
        TestUtilities util =new TestUtilities();
        List<WebElement> item = findAll(itemLocator);
        WebElement specifiedItem = item.get(itemPosition - 1);
        hoverOverElement(specifiedItem);
        List<WebElement> addToCartBtn =findAll(addToCartBtnLocator);
        WebElement specifiedAddToCartBtn = addToCartBtn.get(itemPosition - 1);
        util.sleep(3000);
        specifiedAddToCartBtn.click();
        util.sleep(4000);
        log.info("Product was added to shopping cart");
    }

    //This method get the proceed to checkout button
    public WebElement proceedToCheckoutBtn(){
        WebElement checkOutBtn=find(checkOutBtnLocator);
        return checkOutBtn;
    }

    //This method get the close icon
    public void closeIcon(){
        find(closeIconLocator).click();
    }

    //This method get the search result
    public void checkSearchResult(String keyword){
        WebElement searchResult=find(checkSearchResultLocator);
        String resultMessage= searchResult.getText();
        if(resultMessage.contains("0")){
            log.info("No results were found for"+"'"+keyword+"'");
        }
        else{
            log.info("Some results have been found for "+"'"+keyword+"'");
        }
    }

    //This method get the product label within shopping cart summary section
    public WebElement getProductLb(){
        WebElement productLb = find(productLbtLocator);
        return productLb;
    }

    //This method get the remove icon
    public WebElement removeItem() {
        WebElement removeIcon = find(removeIconLocator);
        return removeIcon;
    }

    //This method get the shopping cart message
    public WebElement getShoppingCartMessage() {
        WebElement shoppingCartMessage = find(shoppingCartMessageLocator);
        return shoppingCartMessage;
    }
}
