package pageObjects;

import base.TestUtilities;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HeaderPageSection extends BasePageObject{
    TestUtilities util=new TestUtilities();
    By searchTbxLocator =By.xpath("//input[@placeholder='Search']");
    By searchBtnLocator =By.xpath("//button[@name='submit_search']");
    By signInLinkLocator =By.cssSelector("#header > div.nav > div > div > nav > div.header_user_info > a");
    By emailAddressTbxLocator =By.cssSelector("#email");
    By passwordLocator =By.cssSelector("#passwd");
    By signInTbxLocator =By.cssSelector("#SubmitLogin > span");
    By myAccountLbLocator =By.cssSelector("#columns > div.breadcrumb.clearfix > span.navigation_page");
    By errorMessageLbLocator =By.cssSelector("#center_column > div.alert.alert-danger > ol > li");



    public HeaderPageSection(WebDriver driver, Logger log) {
        super(driver, log);
    }

    //This method search an specific keyword
    public void searchItem(String Keyword) {
        click(searchTbxLocator);
        type(Keyword,searchTbxLocator);
        click(searchBtnLocator);
        util.sleep(3000);
    }

    //This method clean the search text box
    public void cleanSearchTxb() {
        find(searchTbxLocator).clear();
    }

    //This method login an user
    public void login(String email, String password){
        log.info("Starting log in");
        click(signInLinkLocator);
        type(email,emailAddressTbxLocator);
        type(password,passwordLocator);
        click(signInTbxLocator);

        if (getMyAccountLabel().isDisplayed())
        {
            log.info("Logged with : "+ email);
        }
        else {
            log.info("Login Failed : "+ getErrorMessage().getText());
        }
    }

    //This method get my account label
    public WebElement getMyAccountLabel(){
        WebElement myAccountLabel=find(myAccountLbLocator);
        return myAccountLabel;
    }

    //This method get the error message when login is not valid
    public WebElement getErrorMessage(){
        WebElement errorMessage=find(errorMessageLbLocator);
        return errorMessage;
    }
}
