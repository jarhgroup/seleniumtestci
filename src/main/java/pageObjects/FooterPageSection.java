package pageObjects;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FooterPageSection extends BasePageObject{
    By addressTextLocator =By.cssSelector("section#block_contact_infos  ul > li:nth-of-type(1)");
    By phoneTextLocator =By.cssSelector(".toggle-footer > li:nth-of-type(2) > span");
    By emailTextLocator =By.cssSelector("section#block_contact_infos  .toggle-footer a");

    public FooterPageSection(WebDriver driver, Logger log) {
        super(driver, log);
    }

    //This method get the address information within of the footer section
    public void checkAddressInf() {
        find(addressTextLocator);
        log.info("Address found "+"'"+find(addressTextLocator).getText()+"'");
    }
    //This method get the phone information within of the footer section
    public void checkPhoneInf() {
        find(phoneTextLocator);
        log.info("Address found "+"'"+find(phoneTextLocator).getText()+"'");
    }
    //This method get the email information within of the footer section
    public String checkEmailInf() {
        return getText(emailTextLocator);
    }
}
