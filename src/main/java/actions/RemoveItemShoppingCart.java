package actions;

import base.TestUtilities;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;
import pageObjects.BasePageObject;
import pageObjects.ContentPageSection;

public class RemoveItemShoppingCart extends BasePageObject {
    public RemoveItemShoppingCart(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public void removeItemAction(){
        ContentPageSection contentPageSection= new ContentPageSection(driver,log);
        log.info("Starting to remove Item");
        contentPageSection.removeItem().click();
        TestUtilities utilities = new TestUtilities();
        SoftAssert softAssertion= new SoftAssert();
        softAssertion.assertEquals(contentPageSection.getShoppingCartMessage().getText(),"Your shopping cart is empty.",
                "Shopping cart message is not appearing");
        log.info("Product successfully removed from the shopping cart section");
    }
}
