package actions;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageObjects.BasePageObject;
import pageObjects.ContentPageSection;

public class AddItemShoppingCart extends BasePageObject {
    public AddItemShoppingCart(WebDriver driver, Logger log) {
        super(driver, log);
    }

    public void addItemAction(int itemPosition){
        ContentPageSection contentPageSection= new ContentPageSection(driver,log);
        contentPageSection.addItem(itemPosition);
        contentPageSection.proceedToCheckoutBtn().click();
        Assert.assertEquals(contentPageSection.getProductLb().getText(),"Product",
                "product table is not appearing");
        log.info("Product successfully added to your shopping cart");
    }
}
